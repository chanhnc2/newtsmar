package chanhtuyen.thesis.tsmar.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name="project")
public class Project {
	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@OneToMany(fetch=FetchType.EAGER, cascade={CascadeType.ALL})
	@JoinTable(name="project_comment",
				joinColumns={@JoinColumn(name="project_id")},
				inverseJoinColumns={@JoinColumn(name="comment_id")})
	@OrderBy("id")
	private Set<Comment> comments = new LinkedHashSet<Comment>();
	
	//@ManyToMany(fetch=FetchType.EAGER, mappedBy="projects")
	//@OrderBy("id")
	@ManyToMany(fetch=FetchType.EAGER, cascade={CascadeType.ALL})
	@JoinTable(name="account_project",
				joinColumns={@JoinColumn(name="project_id")},
				inverseJoinColumns={@JoinColumn(name="account_id")})
	@OrderBy("id")
	private Set<Account> accounts = new LinkedHashSet<Account>();
	
	@OneToMany(fetch=FetchType.EAGER, cascade={CascadeType.ALL})
	@JoinTable(name="project_tasktype",
				joinColumns={@JoinColumn(name="project_id")},
				inverseJoinColumns={@JoinColumn(name="tasktype_id")})
	@OrderBy("id")
	private Set<TaskType> taskTypes = new LinkedHashSet<TaskType>();
	
	@OneToMany(fetch=FetchType.EAGER, cascade={CascadeType.ALL})
	@JoinTable(name="project_cumulative",
				joinColumns={@JoinColumn(name="project_id")},
				inverseJoinColumns={@JoinColumn(name="cumulative_id")})
	@OrderBy("id")
	private Set<Cumulative> cumulatives = new LinkedHashSet<Cumulative>();
	
	public Project() {
		
	}
	
	public Project(String name) {
		this.name = name;
	}
	
	public Project(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	public Set<TaskType> getTaskTypes() {
		return taskTypes;
	}

	public void setTaskTypes(Set<TaskType> taskTypes) {
		this.taskTypes = taskTypes;
	}

	public Set<Cumulative> getCumulatives() {
		return cumulatives;
	}

	public void setCumulatives(Set<Cumulative> cumulatives) {
		this.cumulatives = cumulatives;
	}
}
