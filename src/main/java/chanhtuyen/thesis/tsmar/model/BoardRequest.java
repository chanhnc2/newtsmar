package chanhtuyen.thesis.tsmar.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BoardRequest implements Serializable {
	private int columnStartId;
	private int columnTargetId;
	private int taskId;
	private int taskNewPos;
	
	public BoardRequest() {
		
	}

	public int getColumnStartId() {
		return columnStartId;
	}

	public void setColumnStartId(int columnStartId) {
		this.columnStartId = columnStartId;
	}

	public int getColumnTargetId() {
		return columnTargetId;
	}

	public void setColumnTargetId(int columnTargetId) {
		this.columnTargetId = columnTargetId;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public int getTaskNewPos() {
		return taskNewPos;
	}

	public void setTaskNewPos(int taskNewPos) {
		this.taskNewPos = taskNewPos;
	}
}