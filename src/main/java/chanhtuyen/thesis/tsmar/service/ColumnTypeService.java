package chanhtuyen.thesis.tsmar.service;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.ColumnType;

public interface ColumnTypeService {
	public void create(ColumnType columnType);
	public ColumnType get(int id);
	public void update(ColumnType columnType);
	public void delete(int id);
	public List<ColumnType> getAll();
	public ColumnType getLast();
}
