package chanhtuyen.thesis.tsmar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.AccountProjectDao;
import chanhtuyen.thesis.tsmar.model.AccountProject;
import chanhtuyen.thesis.tsmar.service.AccountProjectService;

public class AccountProjectServiceImpl implements AccountProjectService {

	@Autowired
	private AccountProjectDao accountProjectDao;
	
	@Override
	@Transactional
	public void create(AccountProject ap) {
		accountProjectDao.create(ap);
	}

	@Override
	@Transactional
	public void delete(int accountId, int projectId) {
		accountProjectDao.delete(accountId, projectId);
	}

	@Override
	@Transactional
	public void setRule(AccountProject ap, int rule) {
		accountProjectDao.setRule(ap, rule);
	}

	@Override
	@Transactional
	public AccountProject get(AccountProject ap) {
		return accountProjectDao.get(ap);
	}
}
