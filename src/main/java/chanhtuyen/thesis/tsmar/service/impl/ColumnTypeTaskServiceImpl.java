package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.ColumnTypeTaskDao;
import chanhtuyen.thesis.tsmar.model.ColumnTypeTask;
import chanhtuyen.thesis.tsmar.service.ColumnTypeTaskService;

public class ColumnTypeTaskServiceImpl implements ColumnTypeTaskService {
	
	@Autowired
	private ColumnTypeTaskDao columnTypeTaskDao;

	@Override
	@Transactional
	public ColumnTypeTask get(int columnTypeId, int taskId) {
		return columnTypeTaskDao.get(columnTypeId, taskId);
	}

	@Override
	@Transactional
	public void update(int startColumnId, int targetColumnId, int taskId) {
		columnTypeTaskDao.update(startColumnId, targetColumnId, taskId);
	}

	@Override
	@Transactional
	public List<ColumnTypeTask> getList(int columnTypeId) {
		return columnTypeTaskDao.getList(columnTypeId);
	}

	@Override
	@Transactional
	public void updateSort(int columnTypeId, int taskId, int newTaskId) {
		columnTypeTaskDao.updateSort(columnTypeId, taskId, newTaskId);
	}

	@Override
	@Transactional
	public void delete(int taskId) {
		columnTypeTaskDao.delete(taskId);
	}
}
