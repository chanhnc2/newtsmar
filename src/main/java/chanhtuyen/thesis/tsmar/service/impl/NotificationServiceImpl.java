package chanhtuyen.thesis.tsmar.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.NotificationDao;
import chanhtuyen.thesis.tsmar.model.Notification;
import chanhtuyen.thesis.tsmar.service.NotificationService;

public class NotificationServiceImpl implements NotificationService {
	
	@Autowired
	private NotificationDao notificationDao;

	@Override
	@Transactional
	public Notification get(int id) {
		return notificationDao.get(id);
	}

	@Override
	@Transactional
	public void update(Notification notification) {
		notificationDao.update(notification);
	}
}
