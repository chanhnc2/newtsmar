package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.ProjectDao;
import chanhtuyen.thesis.tsmar.model.Cumulative;
import chanhtuyen.thesis.tsmar.model.Project;
import chanhtuyen.thesis.tsmar.service.ProjectService;

public class ProjectServiceImpl implements ProjectService {
	
	@Autowired
	private ProjectDao projectDao;

	@Override
	@Transactional
	public void create(Project project) {
		projectDao.create(project);
	}

	@Override
	@Transactional
	public Project get(int id) {
		return projectDao.get(id);
	}

	@Override
	@Transactional
	public void update(Project project) {
		projectDao.update(project);
	}

	@Override
	@Transactional
	public void delete(int id) {
		projectDao.delete(id);
	}

	@Override
	@Transactional
	public List<Project> getAll() {
		return projectDao.getAll();
	}

	@Override
	@Transactional
	public Project getLast() {
		return projectDao.getLast();
	}

	@Override
	@Transactional
	public Cumulative getLastCumulative(int projectId) {
		return projectDao.getLastCumulative(projectId);
	}

}
