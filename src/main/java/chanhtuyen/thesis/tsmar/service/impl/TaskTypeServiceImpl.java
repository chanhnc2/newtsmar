package chanhtuyen.thesis.tsmar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import chanhtuyen.thesis.tsmar.dao.TaskTypeDao;
import chanhtuyen.thesis.tsmar.model.TaskType;
import chanhtuyen.thesis.tsmar.service.TaskTypeService;

public class TaskTypeServiceImpl implements TaskTypeService {
	
	@Autowired
	private TaskTypeDao taskTypeDao;

	@Override
	@Transactional
	public void create(TaskType taskType) {
		taskTypeDao.create(taskType);
	}

	@Override
	@Transactional
	public TaskType get(int id) {
		return taskTypeDao.get(id);
	}

	@Override
	@Transactional
	public void update(TaskType taskType) {
		taskTypeDao.update(taskType);
	}

	@Override
	@Transactional
	public void delete(int id) {
		taskTypeDao.delete(id);
	}

	@Override
	@Transactional
	public List<TaskType> getAll() {
		return taskTypeDao.getAll();
	}
}
