package chanhtuyen.thesis.tsmar.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.Comment;
import chanhtuyen.thesis.tsmar.model.JsonResult;
import chanhtuyen.thesis.tsmar.model.Notification;
import chanhtuyen.thesis.tsmar.model.Project;
import chanhtuyen.thesis.tsmar.model.Task;
import chanhtuyen.thesis.tsmar.service.AccountService;
import chanhtuyen.thesis.tsmar.service.ProjectService;
import chanhtuyen.thesis.tsmar.service.TaskService;
import chanhtuyen.thesis.tsmar.utils.Const;

@Controller
@RequestMapping("/api/comments")
public class CommentController {
	
	@Autowired
	private SimpMessagingTemplate template;
	private Project resProject = new Project();
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private TaskService taskService;

	@Autowired
	private AccountService accountService;
	
	private void updateResProject() {
		try {
			Integer id = (Integer)Const.session.getAttribute("projectId");
			if (id != null) {
				resProject = projectService.get(id);
				for (Account a: resProject.getAccounts()) {
					a.setNotifications(null);
					a.setProjects(null);
				}
				template.convertAndSend("/topic/project", resProject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// Add comment
	@RequestMapping(value="/addcomment/{taskId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult addComment(HttpSession session,
			@PathVariable(value="taskId") int taskId,
			@RequestBody Comment comment) {
		
		Task task = taskService.get(taskId);
		task.getComments().add(comment);
		taskService.update(task);
		
		String userName = (String)session.getAttribute("userName");
		Notification noti = new Notification();
		noti.setActivated(false);
		noti.setContent(userName + " commented on task " + task.getName());
		
		Integer projectId = (Integer)session.getAttribute("projectId");
		
		if (projectId != null) {
			Project project = projectService.get(projectId);
			
			for (Account a : project.getAccounts()) {
				a.getNotifications().add(noti);
				accountService.update(a);
			}
		}
		
		updateResProject();
		
		JsonResult result = new JsonResult(true, comment);
		
		/*List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);
		return result;
	}
}