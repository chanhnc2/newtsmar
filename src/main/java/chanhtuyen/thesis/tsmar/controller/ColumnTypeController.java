package chanhtuyen.thesis.tsmar.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.ColumnNameRequest;
import chanhtuyen.thesis.tsmar.model.ColumnType;
import chanhtuyen.thesis.tsmar.model.JsonResult;
import chanhtuyen.thesis.tsmar.model.Project;
import chanhtuyen.thesis.tsmar.model.TaskType;
import chanhtuyen.thesis.tsmar.service.AccountService;
import chanhtuyen.thesis.tsmar.service.ColumnTypeService;
import chanhtuyen.thesis.tsmar.service.ProjectService;
import chanhtuyen.thesis.tsmar.service.TaskTypeService;
import chanhtuyen.thesis.tsmar.utils.Const;

@Controller
@RequestMapping(value="/api/columns")
public class ColumnTypeController  {
	
	@Autowired
	private SimpMessagingTemplate template;
	private Project resProject = new Project();
	
	@Autowired
	private ColumnTypeService columnTypeService;
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private TaskTypeService taskTypeService;
	
	@Autowired
	private AccountService accountService;
	
	private void updateResProject() {
		Integer id = (Integer)Const.session.getAttribute("projectId");
		if (id != null) {
			resProject = projectService.get(id);
			for (Account a: resProject.getAccounts()) {
				a.setNotifications(null);
				a.setProjects(null);
			}
			template.convertAndSend("/topic/project", resProject);
		}
	}
	
	@MessageMapping("/addColumnSoc")
	public void addColumnSoc(ColumnNameRequest request) throws Exception {
		TaskType taskType = taskTypeService.get(request.getColumnTypeId());
		ColumnType columnType = new ColumnType();
		columnType.setName(request.getColumnTypeName());
		taskType.getColumnTypes().add(columnType);
		taskTypeService.update(taskType);
		
		updateResProject();
	}
	
	// Add column
	@RequestMapping(value="/addcolumn/{taskTypeId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult addColumn(HttpSession session,
			@PathVariable(value="taskTypeId") int taskTypeId,
			@RequestBody ColumnType columnType) {
		TaskType taskType = taskTypeService.get(taskTypeId);
		taskType.getColumnTypes().add(columnType);
		taskTypeService.update(taskType);
		
		//JsonResult result = new JsonResult(true, columnType);
		JsonResult result = new JsonResult(true, columnTypeService.getLast());
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		//session.setAttribute("listen", false);
		
		return result;
	}
	
	@MessageMapping("/renameColumn")
	public void rename(ColumnNameRequest request) throws Exception {
		if (!request.getColumnTypeName().isEmpty()) {
			ColumnType columnType = columnTypeService.get(request.getColumnTypeId());
			columnType.setName(request.getColumnTypeName());
			columnTypeService.update(columnType);
			
			updateResProject();
		}
	};
	
	// Update name
	@RequestMapping(value="/updatename/{columnTypeId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult updateName(HttpSession session,
			@PathVariable int columnTypeId,
			@RequestBody String columnTypeName) {
		JsonResult result = new JsonResult(true, columnTypeName);
		
		if (!columnTypeName.isEmpty()) {
			ColumnType columnType = columnTypeService.get(columnTypeId);
			columnType.setName(columnTypeName);
			columnTypeService.update(columnType);
		}
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		
		return result;
	}
	
	@MessageMapping("/deleteColumnSoc")
	public void deleteColumnSoc(ColumnNameRequest request) throws Exception {
		columnTypeService.delete(request.getColumnTypeId());
		
		updateResProject();
	}
	
	// Delete column
	@RequestMapping(value="/delete/{columnTypeId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult delete(HttpSession session, @PathVariable int columnTypeId) {
		JsonResult result = new JsonResult(true, "Delete column id: " + columnTypeId);
		columnTypeService.delete(columnTypeId);
		
		/*String userName = (String)session.getAttribute("userName");
		List<Account> accounts = accountService.getAll();
		for (Account a : accounts) {
			if (!userName.equalsIgnoreCase(a.getUserName())) {
				a.setSync(false);
				accountService.update(a);
			}
		}*/
		
		return result;
	}
}
