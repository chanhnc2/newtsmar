package chanhtuyen.thesis.tsmar.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.AccountProject;
import chanhtuyen.thesis.tsmar.model.JsonResult;
import chanhtuyen.thesis.tsmar.model.Project;
import chanhtuyen.thesis.tsmar.service.AccountProjectService;
import chanhtuyen.thesis.tsmar.service.AccountService;
import chanhtuyen.thesis.tsmar.utils.Const;

@Controller
@RequestMapping(value="/api/accounts")
public class AccountController {
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AccountProjectService accountProjectService;
	
	// Get accounts list
	@RequestMapping(method=RequestMethod.GET)
	@ResponseBody
	public List<Account> getAccountsList() {
		List<Account> accounts = accountService.getAll();
		
		for (Account a : accounts) {
			a.setProjects(null);
			a.setNotifications(null);
		}
		
		return accounts;
	}
	
	// Create account
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public JsonResult create(@RequestBody Account account) {
		logger.info("Create a new account");
		
		JsonResult result = new JsonResult(false, null);
		List<Account> accounts = accountService.getAll();
		String password = account.getPassword();
		
		for (Account a : accounts) {
			if (a.getUserName().equalsIgnoreCase(account.getUserName())
				|| a.getEmail().equalsIgnoreCase(account.getEmail())) {
				return result;
			}
		}
		
		result.setSuccess(true);
		
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());

			byte byteData[] = md.digest();
			
			// convert the byte to hex format method 1
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			
			account.setPassword(sb.toString());
			account.setSync(true);
			result.setResult(account);
			accountService.create(account);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	// Load account
	@RequestMapping(value="/loadaccount", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult loadAccount(HttpSession session) {
		JsonResult result = new JsonResult(false, null);
		String userName = (String)session.getAttribute("userName");
		
		if (userName != null) {
			Account account = accountService.getByUserName(userName);
			
			if (account != null) {
				account.setPassword(null);
				
				for (Project p : account.getProjects()) {
					p.setAccounts(null);
				}
			}
			
			result.setSuccess(true);
			result.setResult(account);
		}
		
		return result;
	}
	
	// Login
	@RequestMapping(value="/login", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult login(@RequestBody Account account, HttpSession session) {
		logger.info("Login...");
		JsonResult result;
		Const.session = session;
		MessageDigest md;
		StringBuffer sb = new StringBuffer();
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(account.getPassword().getBytes());

			byte byteData[] = md.digest();
			
			// convert the byte to hex format method 1
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		Account a = accountService.login(account.getUserName(), sb.toString()); 
		
		if (a != null) {
			session.setAttribute("userName", a.getUserName());
			
			a.setPassword(null);
			
			for (Project p : a.getProjects()) {
				p.setAccounts(null);
				p.setComments(null);
				p.setTaskTypes(null);
			}
			
			result = new JsonResult(true, a);
			logger.info("Login successful");
		} else {
			result = new JsonResult(false, a);
			logger.info("Login failed");
		}
		
		session.setAttribute("listen", true);
		
		return result;
	}
	
	// Logout
	@RequestMapping(value="/logout", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult logout(HttpSession session) {
		JsonResult result = null;
		String userName = (String)session.getAttribute("userName");
		
		if (userName != null) {
			session.invalidate();
			
			result = new JsonResult(true, "success");
		} else {
			result = new JsonResult(false, "failed");
		}
		
		return result;
	}
	
	
	// Add project to account
	@RequestMapping(value="/createproject/{accountId}", method=RequestMethod.POST)
	@ResponseBody
	public JsonResult createProject(@PathVariable(value="accountId") int accountId, @RequestBody Project project) {
		JsonResult result = new JsonResult(true, project);
		
		Account account = accountService.get(accountId);
		account.getProjects().add(project);
		accountService.update(account);
		
		return result;
	}

	// Get rule
	@RequestMapping(value = "/getrule", method = RequestMethod.POST)
	@ResponseBody
	public JsonResult getRule(@RequestBody AccountProject ap) {
		JsonResult result = new JsonResult(true, ap);
		
		AccountProject accountProject = accountProjectService.get(ap);
		result.setResult(accountProject);

		return result;
	}
}