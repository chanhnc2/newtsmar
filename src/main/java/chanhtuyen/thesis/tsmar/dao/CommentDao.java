package chanhtuyen.thesis.tsmar.dao;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.Comment;

public interface CommentDao {
	public void create(Comment comment);
	public Comment get(int id);
	public void update(Comment comment);
	public void delete(int id);
	public List<Comment> getAll();
}
