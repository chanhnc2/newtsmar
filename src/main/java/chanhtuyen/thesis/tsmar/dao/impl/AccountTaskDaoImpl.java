package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.AccountTaskDao;
import chanhtuyen.thesis.tsmar.model.AccountTask;

@Repository
public class AccountTaskDaoImpl implements AccountTaskDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(AccountTask accountTask) {
		sessionFactory.getCurrentSession().save(accountTask);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccountTask> getMembers(int taskId) {
		List<AccountTask> members = null;
		String hql = "from AccountTask at where at.task_id = :taskId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("taskId", taskId);
		members = query.list();
		
		return members;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccountTask> getTask(int accountId) {
		List<AccountTask> tasks = null;
		String hql = "from AccountTask at where at.account_id = :accountId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("accountId", accountId);
		tasks = query.list();
		
		return tasks;
	}

	@Override
	public void delete(int accountId, int taskId) {
		String hql = "delete from AccountTask at where at.account_id = :accountId and at.task_id = :taskId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("accountId", accountId);
		query.setParameter("taskId", taskId);
		
		query.executeUpdate();
	}

	@Override
	public void setRule(AccountTask at, int rule) {
		String hql = "update AccountProject ap set ap.rule = :rule where ap.account_id = :accountId and ap.task_id = :taskId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("accountId", at.getAccount_id());
		query.setParameter("taskId", at.getTask_id());
		query.setParameter("rule", rule);
		
		query.executeUpdate();
	}

}
