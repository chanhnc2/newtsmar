package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.TaskDao;
import chanhtuyen.thesis.tsmar.model.Task;

@Repository
public class TaskDaoImpl implements TaskDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(Task task) {
		sessionFactory.getCurrentSession().save(task);
	}

	@Override
	public Task get(int id) {
		return (Task)sessionFactory.getCurrentSession().get(Task.class, id);
	}

	@Override
	public void update(Task task) {
		sessionFactory.getCurrentSession().update(task);
	}

	@Override
	public void delete(int id) {
		Task task = get(id);
		
		if (task != null) {
			sessionFactory.getCurrentSession().delete(task);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from Task").list();
	}

	@Override
	public int getLatestIndex() {
		int size = getAll().size();
		
		if (size > 0)
			return getAll().get(getAll().size() - 1).getTindex();
		else
			return 0;
	}

	@Override
	public Task getLastTask() {
		return getAll().get(getAll().size() - 1);
	}
}
