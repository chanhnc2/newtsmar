package chanhtuyen.thesis.tsmar.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.ProjectDao;
import chanhtuyen.thesis.tsmar.model.Cumulative;
import chanhtuyen.thesis.tsmar.model.Project;

@Repository
public class ProjectDaoImpl implements ProjectDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(Project project) {
		sessionFactory.getCurrentSession().save(project);
	}

	@Override
	public Project get(int id) {
		return (Project)sessionFactory.getCurrentSession().get(Project.class, id);
	}

	@Override
	public void update(Project project) {
		sessionFactory.getCurrentSession().update(project);
	}

	@Override
	public void delete(int id) {
		Project project = get(id);
		
		if (project != null) {
			sessionFactory.getCurrentSession().delete(project);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from Project").list();
	}

	@Override
	public Project getLast() {
		return getAll().get(getAll().size() - 1);
	}

	@Override
	public Cumulative getLastCumulative(int projectId) {
		Project p = get(projectId);
		
		Cumulative cumulative = null;
		for (Cumulative c : p.getCumulatives()) {
			cumulative = c;
		}
		
		return cumulative;
	}
}