package chanhtuyen.thesis.tsmar.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import chanhtuyen.thesis.tsmar.dao.AccountProjectDao;
import chanhtuyen.thesis.tsmar.model.AccountProject;

@Repository
public class AccountProjectDaoImpl implements AccountProjectDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(AccountProject ap) {
		sessionFactory.getCurrentSession().save(ap);
	}

	@Override
	public void delete(int accountId, int projectId) {
		String hql = "delete from AccountProject ap where ap.account_id = :accountId and ap.project_id = :projectId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("accountId", accountId);
		query.setParameter("projectId", projectId);
		
		query.executeUpdate();
	}

	@Override
	public void setRule(AccountProject ap, int rule) {
		String hql = "update AccountProject ap set ap.rule = :rule where ap.account_id = :accountId and ap.project_id = :projectId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("accountId", ap.getAccount_id());
		query.setParameter("projectId", ap.getProject_id());
		query.setParameter("rule", rule);
		
		query.executeUpdate();
	}

	@Override
	public AccountProject get(AccountProject ap) {
		String hql = "from AccountProject ap where ap.account_id = :accountId and ap.project_id = :projectId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("accountId", ap.getAccount_id());
		query.setParameter("projectId", ap.getProject_id());
		
		return (AccountProject)query.uniqueResult();
	}
}
