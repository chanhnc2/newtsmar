package chanhtuyen.thesis.tsmar.dao;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.TaskType;

public interface TaskTypeDao {
	public void create(TaskType taskType);
	public TaskType get(int id);
	public void update(TaskType taskType);
	public void delete(int id);
	public List<TaskType> getAll();
}
