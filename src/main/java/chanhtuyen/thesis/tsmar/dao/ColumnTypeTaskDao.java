package chanhtuyen.thesis.tsmar.dao;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.ColumnTypeTask;

public interface ColumnTypeTaskDao {
	public ColumnTypeTask get(int columnTypeId, int taskId);
	public void delete(int taskId);
	public void update(int startColumnId, int targetColumnId, int taskId);
	public void updateSort(int columnTypeId, int taskId, int newTaskId);
	public List<ColumnTypeTask> getList(int columnTypeId);
}
