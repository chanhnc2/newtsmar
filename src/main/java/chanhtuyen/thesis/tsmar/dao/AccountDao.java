package chanhtuyen.thesis.tsmar.dao;

import java.util.List;

import chanhtuyen.thesis.tsmar.model.Account;
import chanhtuyen.thesis.tsmar.model.Project;

public interface AccountDao {
	public void create(Account account);
	public Account get(int id);
	public Account getByUserName(String userName);
	public void update(Account account);
	public void delete(int id);
	public List<Account> getAll();
	public Account login(String username, String password);
	public Project createProject(Project project);
}
