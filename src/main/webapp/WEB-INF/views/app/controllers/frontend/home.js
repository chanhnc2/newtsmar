define([ '../module' ], function(controllers) {
	'use strict';

	controllers.controller('homeCtrl', function($rootScope, $scope, $location, accountService) {
		// Prepare data
		$rootScope.data = {
			isLogged : false,
			account : {}
		};
		
		var login = function() {
			accountService.login({
				userName: $scope.account.userName,
				password: $scope.account.password
			}).success(function(data, status) {
				if (data.success) {
					console.log('Login successful');

					$rootScope.data.isLogged = true;
					$rootScope.data.account = data.result;
					console.log('data ', $rootScope.data);
					
					$location.path('/myprojects');
				} else {
					console.log('Login fail');
				}
			}).error(function(data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Sign up
		$scope.createAccount = function() {
			$scope.nErr = true;
			accountService.create({
				userName: $scope.account.userName,
				password: $scope.account.password,
				firstName: $scope.account.firstName,
				lastName: $scope.account.lastName,
				email: $scope.account.email
			}).success(function(data, status) {
				/*$scope.account.userName = '';
				$scope.account.password = '';
				$scope.account.firstName = '';
				$scope.account.lastName = '';
				$scope.account.email = '';*/
				$scope.nErr = data.success;
				console.log('Sign up', data);
				if ($scope.nErr) {
					login();
				}
			}).error(function(data, status) {
				console.log('Error ' + status, data);
			});
		};
	
		// Login
		$scope.login = function() {
			accountService.login({
				userName: $scope.account.userName1,
				password: $scope.account.password1
			}).success(function(data, status) {
				if (data.success) {
					console.log('Login successful');
					
					$rootScope.data.isLogged = true;
					$rootScope.data.account = data.result;
					console.log('data ', $rootScope.data);
					
					$location.path('/myprojects');
				} else {
					console.log('Login fail');
				}
			}).error(function(data, status) {
				console.log('Error ' + status, data);
			});
		};
	});
});