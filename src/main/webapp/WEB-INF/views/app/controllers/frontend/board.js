define([ '../module' ], function (controllers) {
	'use strict';

	controllers.controller('boardCtrl', function ($rootScope, $scope, $http, $location, $interval, editableOptions,
			accountService, projectService, taskService, commentService, columnTypeService, notificationService) {
		//Socket
		var socket = new SockJS("/tsmar/ws");
	    var stompClient = Stomp.over(socket);
	    
		// Data
		editableOptions.theme = 'bs3';
		$scope.mouseover = [false, false, false, false];
		$scope.account = accountService.data;
		$scope.colors = [{
			id: 0,
			name: 'None',
			style: 'none'
		},{
			id: 1,
			name: 'Yellow',
			style: 'taskColor-yellow'
		},{
			id: 2,
			name: 'Green',
			style: 'taskColor-green'
		},{
			id: 3,
			name: 'Blue',
			style: 'taskColor-blue'
		},{
			id: 4,
			name: 'Red',
			style: 'taskColor-red'
		},{
			id: 5,
			name: 'Orange',
			style: 'taskColor-orange'
		},{
			id: 6,
			name: 'Purple',
			style: 'taskColor-purple'
		}];
		$scope.selectedColor = $scope.colors[0];
		
		$scope.data = {
			userName: '',
			task: {
				name: '',
				description: '',
				colorId: 1,
				dueDate: ''
			},
			columnTypeId: '',
			taskId: '',
			activeTaskId: '',
			columnType: {
				name: ''
			},
			taskTypeId: '',
			commentData: {
				iTaskType: '',
				iColumnType: '',
				iTask: ''
			},
			// For editing
			selectedTask: {}
		};
		
		$scope.cumulative = {
				date: [],
				nTodo: [],
				nDoing: [],
				nDone: [],
				nProduct: []
		};
		$scope.distribution = [0, 0, 0, 0];
		$scope.creFis = {
			time: ['07-2014'],
			created: [0],
			finished: [0]
		};
		
		$scope.confirmData = {
			message: '',
			kind: '',
			id: '',
			index: '',
			taskId: ''
		};
		
		//-- Start function --//
		$scope.setConfirm = function (message, kind, id, index, taskId) {
			$scope.confirmData.message = message;
			$scope.confirmData.kind = kind;
			$scope.confirmData.id = id;
			$scope.confirmData.index = index;
			$scope.confirmData.taskId = taskId;
		};
		
		$scope.confirmFunc = function (data) {
			if (data.kind == 'column') {
				$scope.deleteColumn(data.id);
			} else if (data.kind == 'memberTask') {
				$scope.removeFromTask({account_id: data.id, task_id: data.taskId}, data.index);
			} else if (data.kind == 'memberProject') {
				$scope.removeMemberFromProject(data.id, data.index);
			} else if (data.kind == 'deleteTask') {
				$scope.deleteTask(data.id);
			}
		};
		
		$scope.getCommentData = function (i1, i2, i3, id) {
			$scope.data.commentData.iTaskType = i1;
			$scope.data.commentData.iColumnType = i2;
			if (i3 == 'addTask') {
				$scope.data.columnTypeId = id;
			} else if (i3 == 'addColumn') {
				$scope.data.taskTypeId = id;
			} else {
				$scope.data.commentData.iTask = i3;
				$scope.data.taskId = id;
			}
		};
		
		// Update board
		var updateBoard = function (boardBody) {
			/*taskService.updateBoard(boardBody).success(function (data, status) {
				console.log('Update board', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});*/
			
			var jsonstr = JSON.stringify({
				columnStartId: boardBody.columnStartId,
				columnTargetId: boardBody.columnTargetId,
				taskId: boardBody.taskId,
				taskNewPos: boardBody.taskNewPos});
			stompClient.send("/app/updateBoardSock", {}, jsonstr);
	        return false;
		};
		
		// Get task id
		var getTaskPos = function (taskTypeIndex, columnIndex) {
			var pos = null;
			var taskLength = $scope.project.taskTypes[taskTypeIndex].columnTypes[columnIndex].tasks.length;
			for (var i = 0; i < taskLength; i++) {
				var t = $scope.project.taskTypes[taskTypeIndex].columnTypes[columnIndex].tasks[i];
				
				if (t.id == $scope.data.activeTaskId) {
					pos = i;
					
					break;
				}
			}
			
			return pos;
		};
		
		// Get accounts list
		var getAccountList = function () {
			accountService.getAccountsList().success(function (data, status) {
				$scope.accounts = data;
				
				console.log('Accounts ', data);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Logout
		$scope.logout = function () {
			accountService.logout().success(function (data, status) {
				$location.path('/');
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Render project
		var renderProject = function (frame) {
			var temp = JSON.parse(frame.body);
			if (temp != null) {
				$scope.project = temp;
				$scope.members = $scope.project.accounts;
				
				$scope.project.cumulatives.forEach(function (cumulative) {
					$scope.cumulative.date.push(cumulative.date);
					$scope.cumulative.nTodo.push(cumulative.nTodo);
					$scope.cumulative.nDoing.push(cumulative.nDoing);
					$scope.cumulative.nDone.push(cumulative.nDone);
					$scope.cumulative.nProduct.push(cumulative.nProduct);
				});
				
				var totalTask = 0;
				var iTask = 0;
				$scope.project.taskTypes.forEach(function (taskType) {
					taskType.columnTypes.forEach(function (columnType) {
						$scope.slideWidth++;
						
						columnType.tasks.forEach(function (task) {
							task.showComment = false;
							task.comment = {content: ''};
							$scope.distribution[iTask]++;
							totalTask++;
						});
					});
					iTask++;
				});
				
				$scope.sortableOptions = {
						placeholder: 'task',
						connectWith: '.tasks-container',
						stop: function (e, ui) {
							if (ui.item.sortable.droptarget.attr('id') != undefined) {
								updateBoard({
									columnStartId: e.target.id,
									columnTargetId: ui.item.sortable.droptarget.attr('id'),
									taskId: $scope.data.activeTaskId,
									taskNewPos: getTaskPos(ui.item.sortable.droptarget.attr('task-type-index'),
										ui.item.sortable.droptarget.attr('column-index'))
								});
								
								console.log('Update board', e.target.id + ' to ' + ui.item.sortable.droptarget.attr('id') + ' pos: '
										+ getTaskPos(ui.item.sortable.droptarget.attr('task-type-index'),
												ui.item.sortable.droptarget.attr('column-index')) + '-taskId ' + $scope.data.activeTaskId);
								
								taskService.statistic(ui.item.sortable.droptarget.attr('task-type-index')).success(function (dat, stat) {
									console.log('Statistic', dat.result);
								}).error(function (dat, stat) {
									console.log('Error ' + status, dat);
								});
							}
						}
					};
				$scope.$apply();
			}
		};
		
		// Load project
		var loadProject = function (projectBody) {
			projectService.loadProject(projectBody).success(function (data, status) {
				$scope.project = data.result;
				$scope.members = $scope.project.accounts;
				
				$scope.project.cumulatives.forEach(function (cumulative) {
					$scope.cumulative.date.push(cumulative.date);
					$scope.cumulative.nTodo.push(cumulative.nTodo);
					$scope.cumulative.nDoing.push(cumulative.nDoing);
					$scope.cumulative.nDone.push(cumulative.nDone);
					$scope.cumulative.nProduct.push(cumulative.nProduct);
				});
				
				var totalTask = 0;
				var iTask = 0;
				$scope.project.taskTypes.forEach(function (taskType) {
					taskType.columnTypes.forEach(function (columnType) {
						$scope.slideWidth++;
						
						columnType.tasks.forEach(function (task) {
							task.showComment = false;
							task.comment = {content: ''};
							$scope.distribution[iTask]++;
							totalTask++;
						});
					});
					iTask++;
				});
				
				// Prepare chart
				var rate = 0;
				for (var i = 0; i < 4; i++) {
					rate = $scope.distribution[i] * 100.0 / totalTask;
					$scope.creFis.created[0] += $scope.distribution[i];
					if (i < 3) {
						$scope.distribution[i] = rate.toFixed(2);
					}
				}
				$scope.creFis.finished[0] += $scope.distribution[3];
				$scope.distribution[3] = rate.toFixed(2);
				
				$scope.sortableOptions = {
					placeholder: 'task',
					connectWith: '.tasks-container',
					stop: function (e, ui) {
						if (ui.item.sortable.droptarget.attr('id') != undefined) {
							updateBoard({
								columnStartId: e.target.id,
								columnTargetId: ui.item.sortable.droptarget.attr('id'),
								taskId: $scope.data.activeTaskId,
								taskNewPos: getTaskPos(ui.item.sortable.droptarget.attr('task-type-index'),
									ui.item.sortable.droptarget.attr('column-index'))
							});
							
							console.log('Update board', e.target.id + ' to ' + ui.item.sortable.droptarget.attr('id') + ' pos: '
									+ getTaskPos(ui.item.sortable.droptarget.attr('task-type-index'),
											ui.item.sortable.droptarget.attr('column-index')) + '-taskId ' + $scope.data.activeTaskId);
							
							taskService.statistic(ui.item.sortable.droptarget.attr('task-type-index')).success(function (dat, stat) {
								console.log('Statistic', dat.result);
							}).error(function (dat, stat) {
								console.log('Error ' + status, dat);
							});
						}
					}
				};
				
				// Charts
				// Cumulative
				/*$scope.chartFlow = {
						title : {
							text : "Cumulative Flow Chart"
						},
						legend : {
							position : "bottom"
						},
						seriesDefaults : {
							type : "area",
							stack : true
						},
						series : [{
							name : "Idea",
							data : $scope.cumulative.nTodo
						}, {
							name : "Todo",
							data : $scope.cumulative.nDoing
						}, {
							name : "In progress",
							data : $scope.cumulative.nDone
						}, {
							name : "Done",
							data : $scope.cumulative.nProduct
						}],
						valueAxis : {
							labels : {
								format : "{0}"
							},
							line : {
								visible : false
							},
							axisCrossingValue : -10,
							max : 15
						},
						categoryAxis : {
							categories : $scope.cumulative.date,
							majorGridLines : {
								visible : false
							}
						},
						tooltip : {
							visible : true,
							format : "{0}",
							template : "#= series.name #: #= value #"
						}
				};*/
				
				// Distribution
				$scope.chartDistribution =  {
						title: {
				             position: "bottom",
				             text: "Project distribution"
				         },
				         legend: {
				             visible: false
				         },
				         chartArea: {
				             background: ""
				         },
				         seriesDefaults: {
				             labels: {
				                 visible: true,
				                 background: "transparent",
				                 template: "#= category #: #= value#%"
				             }
				         },
				         series: [{
				             type: "pie",
				             startAngle: 150,
				             data: [{
				                 category: "Idea",
				                 value: $scope.distribution[0],
				                 color: "#9de219"
				             }, {
				                 category: "Todo",
				                 value: $scope.distribution[1],
				                 color: "#90cc38"
				             }, {
				                 category: "In Progress",
				                 value: $scope.distribution[2],
				                 color: "#068c35"
				             }, {
				                 category: "Done",
				                 value: $scope.distribution[3],
				                 color: "#006634"
				             }]
				         }],
				         tooltip: {
				             visible: true,
				             format: "{0}%"
				         }
				};
				
				// End chart /////////////////////////////////////////////
				
				console.log('Project', $scope.project);
			}).error(function(data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Add task
		$scope.addTask = function (columnTypeId, taskBody) {
			taskBody.colorId = $scope.selectedColor.id;
			var jsonstr = JSON.stringify({
				columnTypeId: columnTypeId,
				name: taskBody.name,
				description: taskBody.description,
				colorId: taskBody.colorId,
				dueDate: taskBody.dueDate
			});
			stompClient.send("/app/addTaskSock", {}, jsonstr);
			return false;
			/*taskService.addTask(columnTypeId, taskBody).success(function (data, status) {
				$scope.project
				.taskTypes[$scope.data.commentData.iTaskType]
				.columnTypes[$scope.data.commentData.iColumnType]
				.tasks.push(data.result);
				
				taskBody.name = '';
				taskBody.description = '';
				taskBody.dueDate = '';
				
				console.log('Add task', data.result);
				
				taskService.statistic($scope.data.commentData.iTaskType).success(function (dat, stat) {
					console.log('Statistic', dat.result);
				}).error(function (dat, stat) {
					console.log('Error ' + status, dat);
				});
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});*/
		};
		
		// Delete task
		$scope.deleteTask = function (taskId) {
			taskService.deleteTask(taskId).success(function (data, status) {
				loadProject({id: $rootScope.projectId});
				console.log('Delete task', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Add comment
		$scope.addComment = function (pos, taskId, commentBody) {
			commentService.addComment(taskId, commentBody).success(function(data, status) {
				if (pos == 1) {
					var temp = {content: commentBody.content, username: commentBody.username};
					$scope.project
					.taskTypes[$scope.data.commentData.iTaskType]
					.columnTypes[$scope.data.commentData.iColumnType]
					.tasks[$scope.data.commentData.iTask].comments.push(temp);
						
					commentBody.content = '';
				} else {
					var temp = {content: commentBody.content, username: commentBody.username};
					$scope.data.selectedTask.comments.push(temp);
					
					$scope.commentContent = '';
				}
				console.log('Add comment', data.result);
			}).error(function(data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Add column
		$scope.addColumn = function (taskTypeId, columnBody) {
			if (columnBody.name != '') {
				/*columnTypeService.addColumn(taskTypeId, columnBody).success(function (data, status) {
					var temp = {name: columnBody.name, tasks: []};
					$scope.project
					.taskTypes[$scope.data.commentData.iTaskType]
					.columnTypes.push(data.result);
					
					columnBody.name = '';
					console.log('Add column', data.result);
				}).error(function (data, status) {
					console.log('Error ' + status, data);
				});*/
				var jsonstr = JSON.stringify({ 'columnTypeId': taskTypeId, 'columnTypeName': columnBody.name });
				stompClient.send("/app/addColumnSoc", {}, jsonstr);
				columnBody.name = '';
		        return false;
			}
		};
		
		// Assign to project
		$scope.assignToProject = function (accountBody) {
			projectService.assign(accountBody).success(function (data, status) {
				$scope.members.push(data.result);
				$scope.searchText = "";
				console.log('Assigned account', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Remove member from project
		$scope.removeMemberFromProject = function (accountId, index) {
			projectService.removeMember(accountId).success(function (data, status) {
				$scope.members.splice(index, 1);
				
				console.log('Remove member', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Assign member to task
		$scope.assignToTask = function (accountTaskBody) {
			taskService.assignMember(accountTaskBody).success(function (data, status) {
				$scope.taskMembers.push(data.result);
				$scope.searchText = "";
				console.log('Assign member to task', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Get task members
		$scope.getTaskMembers = function (accountTaskBody, task) {
			$scope.data.selectedTask = task;
			
			taskService.getMembers(accountTaskBody).success(function (data, status) {
				$scope.taskMembers = data.result;
				
				console.log('Task members', $scope.taskMembers);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Remove member from task
		$scope.removeFromTask = function (accountTaskBody, index) {
			taskService.removeMember(accountTaskBody).success(function (data, status) {
				$scope.taskMembers.splice(index, 1);
				
				console.log('Remove member from task', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Check if exist member
		$scope.checkMember = function (account) {
			if ($scope.members != undefined) {
				for (var i = 0; i < $scope.members.length; i++) {
					if (account.id == $scope.members[i].id) {
						return true;
					}
				}
			}
			
			return false;
		};
		
		// Update notifications
		$scope.updateNotiStatus = function (notiIndex, notiId) {
			notificationService.updateStatus(notiId).success(function (data, status) {
				$scope.account.nUnactivedNoti--;
				$scope.account.notifications[notiIndex].actived = true;
				console.log('Notification', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Get rule
		$scope.getRule = function (accountProjectBody) {
			accountService.getRule(accountProjectBody).success(function (data, status) {
				$scope.rule = data.result.rule;
				console.log('Get rule', data);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Update task name
		$scope.updateTaskName = function (nameTask, taskId) {
			taskService.updateTaskName(nameTask, taskId).success(function (data, status) {
				console.log('Update task name', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Update task description
		$scope.updateTaskDescription = function (descriptionTask, taskId) {
			taskService.updateTaskDescription(descriptionTask, taskId).success(function (data, status) {
				console.log('Update description name', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Update task color
		$scope.updateTaskColor = function (colorId, taskId) {
			taskService.updateTaskColor(colorId, taskId).success(function (data, status) {
				console.log('Update color', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Update task due date
		$scope.updateTaskDueDate = function (dueDate, taskId) {
			taskService.updateTaskDueDate(dueDate, taskId).success(function (data, status) {
				console.log('Update due date', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		};
		
		// Update column name
		$scope.updateColumnName = function (columnTypeId, newName) {
			/*columnTypeService.updateName(columnTypeId, newName).success(function (data, status) {
				console.log('Update column name ', data);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});*/
			var jsonstr = JSON.stringify({ 'columnTypeId': columnTypeId, 'columnTypeName': newName });
			stompClient.send("/app/renameColumn", {}, jsonstr);
	        return false;
		};
		
		// Delete column
		$scope.deleteColumn = function (columnTypeId) {
			/*columnTypeService.deleteColumn(columnTypeId).success(function (data, status) {
				$scope.project
				.taskTypes[$scope.data.commentData.iTaskType]
				.columnTypes.splice($scope.data.commentData.iColumnType, 1);
				loadProject({id: $rootScope.projectId});
				console.log('Delete column', data.result);
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});*/
			var jsonstr = JSON.stringify({ 'columnTypeId': columnTypeId });
			stompClient.send("/app/deleteColumnSoc", {}, jsonstr);
	        return false;
		};
		//-- End function --//
		
		var connectCallback = function() {
		      stompClient.subscribe('/topic/project', renderProject);
		};
		
		// Callback function to be called when stomp client could not connect to server
	    var errorCallback = function(error) {
	      alert(error.headers.message);
	    };

	    // Connect to server via websocket
	    stompClient.connect("guest", "guest", connectCallback, errorCallback);
		loadProject({id: $rootScope.projectId});
		getAccountList();
		
		/*$interval(function () {
			projectService.listen().success(function (data, status) {
				if (data.result == false) {
					loadProject({id: $rootScope.projectId});
				}
			}).error(function (data, status) {
				console.log('Error ' + status, data);
			});
		}, 1000);*/
		
		///////////////////////////////////////////////////////////////////////////////
		// Cumulative
		$scope.chartFlow = {
				title : {
					text : "Cumulative Flow Chart"
				},
				legend : {
					position : "bottom"
				},
				seriesDefaults : {
					type : "area",
					stack : true
				},
				series : [{
					name : "Idea",
					data : $scope.cumulative.nTodo,
					color: "#ff0000"
				}, {
					name : "Todo",
					data : $scope.cumulative.nDoing,
					color: "#00ff00"
				}, {
					name : "In progress",
					data : $scope.cumulative.nDone,
					color: "#0000ff"
				}, {
					name : "Done",
					data : $scope.cumulative.nProduct,
					color: "#00ffff"
				}],
				valueAxis : {
					labels : {
						format : "{0}"
					},
					line : {
						visible : false
					},
					axisCrossingValue : -10,
					max : 50
				},
				categoryAxis : {
					categories : $scope.cumulative.date,
					majorGridLines : {
						visible : false
					}
				},
				tooltip : {
					visible : true,
					format : "{0}",
					template : "#= series.name #: #= value #"
				}
		};
		
		// Distribution
		/*$scope.chartDistribution =  {
				title: {
		             position: "bottom",
		             text: "Project distribution"
		         },
		         legend: {
		             visible: false
		         },
		         chartArea: {
		             background: ""
		         },
		         seriesDefaults: {
		             labels: {
		                 visible: true,
		                 background: "transparent",
		                 template: "#= category #: #= value#%"
		             }
		         },
		         series: [{
		             type: "pie",
		             startAngle: 150,
		             data: [{
		                 category: "Idea",
		                 value: $scope.distribution[0],
		                 color: "#9de219"
		             }, {
		                 category: "Todo",
		                 value: $scope.distribution[1],
		                 color: "#90cc38"
		             }, {
		                 category: "In Progress",
		                 value: $scope.distribution[2],
		                 color: "#068c35"
		             }, {
		                 category: "Done",
		                 value: $scope.distribution[3],
		                 color: "#006634"
		             }]
		         }],
		         tooltip: {
		             visible: true,
		             format: "{0}%"
		         }
		};*/
		// Chart finished
		$scope.chartFinished = {
				title: {
					position: "bottom",
		            text: "Created and Finished Tasks"
		         },
		         legend: {
		             position: "top"
		         },
		         seriesDefaults: {
		             type: "column"
		         },
		         series: [{
		             name: "Created",
		             data: $scope.creFis.created
		         }, {
		             name: "Finished",
		             data: $scope.creFis.finished
		         }],
		         valueAxis: {
		             labels: {
		                 format: "{0}"
		             },
		             line: {
		                 visible: false
		             },
		             axisCrossingValue: 0
		         },
		         categoryAxis: {
		             categories: $scope.creFis.time,
		             line: {
		                 visible: false
		             }
		         },
		         tooltip: {
		             visible: true,
		             format: "{0}",
		             template: "#= series.name #: #= value #"
		         }
		};
	});
});