define(['./module'], function (services) {
	'use strict';
	
	services.service('taskService', function ($http) {
		this.baseUrl = 'api/tasks/';
		
		// Add task
		this.addTask = function (columnTypeId, taskBody) {
			return $http.post(this.baseUrl + 'addtask/' + columnTypeId, taskBody);
		};
		
		// Delete task
		this.deleteTask = function (taskId) {
			return $http.post(this.baseUrl + 'delete/' + taskId, {});
		};
		
		// Update board
		this.updateBoard = function (boardRequestBody) {
			return $http.post(this.baseUrl + 'updateboard', boardRequestBody);
		};
		
		// Update name
		this.updateTaskName = function (nameTask, taskId) {
			return $http.post(this.baseUrl + 'updatename/' + taskId, nameTask);
		};
		
		// Update description
		this.updateTaskDescription = function (descriptionTask, taskId) {
			return $http.post(this.baseUrl + 'updatedescription/' + taskId, descriptionTask);
		};
		
		// Update color
		this.updateTaskColor = function (colorId, taskId) {
			return $http.post(this.baseUrl + 'updatecolor/' + taskId, colorId);
		};
		
		// Update due date
		this.updateTaskDueDate = function (dueDate, taskId) {
			return $http.post(this.baseUrl + 'updateduedate/' + taskId, dueDate);
		};
		
		// Assign member
		this.assignMember = function (accountTaskBody) {
			return $http.post(this.baseUrl + 'assignmember', accountTaskBody);
		};
		
		// Remove member
		this.removeMember = function (accountTaskBody) {
			return $http.post(this.baseUrl + 'removemember', accountTaskBody);
		};
		
		// Get members
		this.getMembers = function (accountTaskBody) {
			return $http.post(this.baseUrl + 'getmembers', accountTaskBody);
		};
		
		// Statistic
		this.statistic = function (type) {
			return $http.post(this.baseUrl + 'statistic/' + type, {});
		};
	});
});