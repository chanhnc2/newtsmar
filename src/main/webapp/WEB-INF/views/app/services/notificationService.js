define(['./module'], function (services) {
	'use strict';
	
	services.service('notificationService', function ($http) {
		this.baseUrl = 'api/notifications/';
		
		// Update status
		this.updateStatus = function (notiId) {
			return $http.get(this.baseUrl + 'update/' + notiId);
		};
	});
});