define(['./module'], function (services) {
	'use strict';
	
	services.service('columnTypeService', function ($http) {
		this.baseUrl = 'api/columns/';
		
		// Add column
		this.addColumn = function (taskTypeId, columnBody) {
			return $http.post(this.baseUrl + 'addcolumn/' + taskTypeId, columnBody);
		};
		
		// Update name
		this.updateName = function (columnTypeId, newName) {
			return $http.post(this.baseUrl + 'updatename/' + columnTypeId, newName);
		};
		
		// Delete column
		this.deleteColumn = function (columnTypeId) {
			return $http.post(this.baseUrl +'delete/' + columnTypeId, {});
		};
	});
});