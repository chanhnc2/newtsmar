define(['./module'], function (services) {
	'use strict';
	
	services.service('projectService', function ($http) {
		this.baseUrl = 'api/projects';
		
		// Create project
		this.createProject = function (input) {
			return $http.post(this.baseUrl + '/create', input);
		};
		
		// Load projects
		this.loadProjects = function () {
			return $http.get(this.baseUrl + '/loadprojects');
		};
		
		// Load project
		this.loadProject = function (projectBody) {
			return $http.post(this.baseUrl + '/loadproject', projectBody);
		};
		
		// Get members
		this.getMembers = function () {
			return $http.get(this.baseUrl + '/getmembers');
		};
		
		// Assign to project
		this.assign = function (accountBody) {
			return $http.post(this.baseUrl + '/assigntoproject', accountBody);
		};
		
		// Remove member
		this.removeMember = function (accountId) {
			return $http.post(this.baseUrl + '/removemember/' + accountId, {});
		};
		
		// Listener
		this.listen = function () {
			return $http.post(this.baseUrl + '/listener', {});
		};
	});
});